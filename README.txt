Daily data upload from API to Bigquery:

1. Cloud functions (Python) were created (see bigquery.png for screenshot and "Cloud functions" folder for code):

    - installs_api_to_bq.py
    - costs_api_to_bq.py
    - orders_api_to_bq.py
    - events_api_to_bq.py

The functions extract relevant data from the API responses and format it for BigQuery ingestion.

Each function has its own topic:

    - installs_data_api_to_bq
    - costs_data_api_to_bq
    - orders_data_api_to_bq
    - ievents_data_api_to_bq
    
2. To execute cloud functions daily Cloud Scheduler was used. 4 jobs were created (see cloud_scheduler.png for screenshot):

    - installs_update
    - costs_update
    - orders_update
    - events_update

which triggers every day with 3 minutes difference to avoid overloading the API with simultaneous requests.
I schedule jobs at the same day for two days ago to make sure all data in API is updated, it can be modified based on API update time.

3. Data is uploaded to BigQuery tables (see "Tables Schema" folder for db tables schema):

    -installs
    -costs
    -orders
    -events

4. View to analyse marketing performance where created. SQL code is located in the "View" folder and view schema in the "Tables Schema" folder under "marketing_performance_schema" name.