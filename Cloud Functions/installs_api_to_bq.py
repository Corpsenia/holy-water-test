import requests
import pandas as pd
import json
from google.cloud import bigquery
from datetime import date, timedelta


def get_install_data(api_url, method, yesterday, api_key=None):
    endpoint_url = f"{api_url}{method}?date={yesterday}"

    headers = {"Authorization": f"{api_key}"
               }
    response = requests.get(endpoint_url, headers=headers)
    response.raise_for_status()
    myjson = response.json()

    records = json.loads(myjson['records'])
    df = pd.DataFrame(records)
    df['install_time'] = pd.to_datetime(df['install_time'])

    project_id = "holy-water-test-421313"
    table_id = "holy-water-test-421313.holy_water.installs"
    client = bigquery.Client(project=project_id)

    job_config = bigquery.LoadJobConfig(
        write_disposition=bigquery.WriteDisposition.WRITE_APPEND
    )

    job = client.load_table_from_dataframe(
        df, table_id, job_config=job_config
    )
    job.result()


API_URL = "https://us-central1-passion-fbe7a.cloudfunctions.net/dzn54vzyt5ga"
API_KEY = "gAAAAABmJ6u7jJvTnflqguPuFZLP4hOOVVuC59V9mWqKc5RNuS-hhZrUv6RebQZWiNZvcN9xjLlxSW1Ul9U-mcJick4jzir_G6AhXigS4t5axTby7V03_GXt2weZnsIWajhqR4yGdy7A"
DATE = (date.today() - timedelta(days=3)).strftime("%Y-%m-%d")
METHOD = "/installs"

def main(data, context):
    get_install_data(API_URL, METHOD, DATE, API_KEY)
