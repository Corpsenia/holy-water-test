import requests
from google.cloud import bigquery
from datetime import date
from datetime import timedelta
import pyarrow.parquet as pq
from io import BytesIO


def get_orders_data(api_url, method, yesterday, api_key=None):
    endpoint_url = f"{api_url}{method}?date={yesterday}"

    headers = {"Authorization": f"{api_key}"
               }

    response = requests.get(endpoint_url, headers=headers)
    response.raise_for_status()
    data_buffer = BytesIO(response.content)
    table = pq.read_table(data_buffer)
    dataset_path = f"orders_{yesterday}"
    pq.write_to_dataset(table, root_path=dataset_path,
                        partition_cols=['transaction_id'])
    data = pq.read_table(dataset_path).to_pandas()
    table = data.rename(
        columns={"iap_item.name": "iap_item_name", "iap_item.price": "iap_item_price", "discount.code": "discount_code",
                 "discount.amount": "discount_amount"})


    project_id = "holy-water-test-421313"
    dataset_id = "holy-water-test-421313.holy_water"
    table_id = "holy-water-test-421313.holy_water.orders"
    client = bigquery.Client(project=project_id)

    job_config = bigquery.LoadJobConfig(
        write_disposition=bigquery.WriteDisposition.WRITE_APPEND
    )

    job = client.load_table_from_dataframe(table, table_id, job_config=job_config)
    job.result()


API_URL = "https://us-central1-passion-fbe7a.cloudfunctions.net/dzn54vzyt5ga"
API_KEY = "gAAAAABmJ6u7jJvTnflqguPuFZLP4hOOVVuC59V9mWqKc5RNuS-hhZrUv6RebQZWiNZvcN9xjLlxSW1Ul9U-mcJick4jzir_G6AhXigS4t5axTby7V03_GXt2weZnsIWajhqR4yGdy7A"
DATE = (date.today() - timedelta(days=2)).strftime("%Y-%m-%d")
METHOD = "/orders"


def main(data, context):
    get_orders_data(API_URL, METHOD, DATE, API_KEY)
