import requests
import pandas as pd
import json
from google.cloud import bigquery
from datetime import datetime
from datetime import timedelta


def get_costs_data(api_url, method, api_key=None, dimensions=None):
    today = (datetime.today() - timedelta(days=2))
    past_7_days = [today - timedelta(days=i) for i in range(7)]
    project_id = "holy-water-test-421313"
    table_id = "holy-water-test-421313.holy_water.costs"
    client = bigquery.Client(project=project_id)

    for date in past_7_days:
        params = {"date": date.strftime('%Y-%m-%d'),
                  "dimensions": dimensions}
        headers = {"Authorization": f"{api_key}"}

        response = requests.get(f"{api_url}{method}?", params=params, headers=headers)
        response.raise_for_status()

    # Convert string to json
    string_data = response.content.decode('utf-8')
    lines = string_data.splitlines()
    # Skip the column names
    data_lines = lines[1:]
    # Create a list of dictionaries, one for each data row
    json_data = []
    for line in data_lines:
        values = line.split('\t')

        # Create a dictionary with column names as keys and values as elements
        for date in past_7_days:
            row_data = {
                'date': date.strftime('%Y-%m-%d'),
                'ad_group': values[0],
                'location': values[1],
                'keyword': values[2],
                'campaign': values[3],
                'landing_page': values[4],
                'channel': values[5],
                'ad_content': values[6],
                'medium': values[7],
                'cost': float(values[8])  # Convert cost to float
            }
            json_data.append(row_data)

    # Delete rows from the costs table with the same date
    for date in past_7_days:
        query_job = client.query(f"""
        DELETE FROM holy-water-test-421313.holy_water.costs
        WHERE date = '{date.strftime('%Y-%m-%d')}'
        """)
        query_job.result()

    # Convert the list of dictionaries to JSON string
    json_string = json.dumps(json_data)
    records = json.loads(json_string)
    df = pd.DataFrame(records)
    df['date'] = pd.to_datetime(df['date'])

    job_config = bigquery.LoadJobConfig(
        write_disposition=bigquery.WriteDisposition.WRITE_APPEND
    )

    job = client.load_table_from_dataframe(
        df, table_id, job_config=job_config
    )
    job.result()


API_URL = "https://us-central1-passion-fbe7a.cloudfunctions.net/dzn54vzyt5ga"
API_KEY = "gAAAAABmJ6u7jJvTnflqguPuFZLP4hOOVVuC59V9mWqKc5RNuS-hhZrUv6RebQZWiNZvcN9xjLlxSW1Ul9U-mcJick4jzir_G6AhXigS4t5axTby7V03_GXt2weZnsIWajhqR4yGdy7A"
METHOD = "/costs"
specific_dimensions = "location,channel,medium,campaign,keyword,ad_content,ad_group,landing_page"
specific_dimensions = "location,channel,medium,campaign,keyword,ad_content,ad_group,landing_page"


def main(data, context):
    get_costs_data(API_URL, METHOD, API_KEY, dimensions=specific_dimensions)
