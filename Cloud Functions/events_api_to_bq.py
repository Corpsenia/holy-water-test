import requests
import pandas as pd
import json
from google.cloud import bigquery
from datetime import date, datetime
from datetime import timedelta


def get_events_data(api_url, method, yesterday, api_key=None):
    endpoint_url = f"{api_url}{method}?date={yesterday}"

    headers = {"Authorization": f"{api_key}"
               }

    response = requests.get(endpoint_url, headers=headers)
    response.raise_for_status()

    myjson = response.json()
    records = json.loads(myjson['data'])
    normalize_data = pd.json_normalize(records)
    df = pd.DataFrame(normalize_data)
    next_page = myjson.get("next_page")

    try:
        while next_page:
            # Download subsequent pages using next_page parameter and append data
            next_page_response = requests.get(f"{endpoint_url}&next_page={next_page}", headers=headers)
            next_page_response.raise_for_status()
            next_page_json = next_page_response.json()
            next_page_records = json.loads(next_page_json['data'])
            normalize_page_data = pd.json_normalize(next_page_records)
            df = df.append(pd.DataFrame(normalize_page_data), ignore_index=True)
            next_page = next_page_json.get("next_page")
        return df
    except Exception as e:
        print(f"Error downloading page {next_page}: {e}")

    data = df.rename(
        columns={"user_params.os": "user_params_os", "user_params.brand": "user_params_brand",
                 "user_params.model": "user_params_model",
                 "user_params.model_number": "user_params_model_number",
                 "user_params.specification": "user_params_specification",
                 "user_params.transaction_id": "user_params_transaction_id",
                 "user_params.campaign_name": "user_params_campaign_name", "user_params.source": "user_params_source",
                 "user_params.medium": "user_params_medium",
                 "user_params.term": "user_params_term", "user_params.context": "user_params_context",
                 "user_params.gclid": "user_params_gclid",
                 "user_params.dclid": "user_params_dclid", "user_params.srsltid": "user_params_srsltid",
                 "user_params.is_active_user": "user_params_is_active_user",
                 "user_params.marketing_id": "user_params_marketing_id"})

    data['event_time'] = pd.to_datetime(data['event_time'], unit='ms')


    data['event_time'] = data['event_time'].replace('', 0)
    data['session_number'] = data['session_number'].replace('', 0)
    data['value'] = data['value'].replace('', 0.0)
    data['state'] = data['state'].replace('', 0.0)
    data['engagement_time_msec'] = data['engagement_time_msec'].replace('', 0.0)
    data['model_number'] = data['model_number'].replace('', 0.0)
    data['place'] = data['place'].replace('', 0.0)

    project_id = "holy-water-test-421313"
    dataset_id = "holy-water-test-421313.holy_water"
    table_id = "holy-water-test-421313.holy_water.events"
    client = bigquery.Client(project=project_id)



    job_config = bigquery.LoadJobConfig(
        write_disposition=bigquery.WriteDisposition.WRITE_APPEND
    )

    job = client.load_table_from_dataframe(
        data, table_id, job_config=job_config
    )
    job.result()


API_URL = "https://us-central1-passion-fbe7a.cloudfunctions.net/dzn54vzyt5ga"
API_KEY = "gAAAAABmJ6u7jJvTnflqguPuFZLP4hOOVVuC59V9mWqKc5RNuS-hhZrUv6RebQZWiNZvcN9xjLlxSW1Ul9U-mcJick4jzir_G6AhXigS4t5axTby7V03_GXt2weZnsIWajhqR4yGdy7A"
DATE = (date.today() - timedelta(days=2)).strftime("%Y-%m-%d")
METHOD = "/events"

def main(data, context):
    get_events_data(API_URL, METHOD, DATE, API_KEY)
