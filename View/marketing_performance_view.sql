CREATE OR REPLACE VIEW holy-water-test-421313.holy_water.marketing_performance AS
WITH CostData AS (
  SELECT
    campaign,
    channel,
    medium,
    ad_content,
    ad_group,
    SUM(cost) AS total_cost
  FROM `holy-water-test-421313.holy_water.costs`
  GROUP BY campaign, channel, medium, ad_content, ad_group
),

InstallData AS (
  SELECT
    install_time,
    campaign,
    channel,
    medium,
    ad_content,
    ad_group,
    marketing_id,
    COUNT(*) AS total_installs
  FROM `holy-water-test-421313.holy_water.installs`
  GROUP BY install_time, campaign, channel, medium, ad_content, ad_group, marketing_id
),

OrderData AS (
  SELECT
    o.event_time,
    iap_item_price - (fee + tax + discount_amount) as order_revenue,
    e.user_id,
    user_params_marketing_id as marketing_id,
    o.category
  FROM holy-water-test-421313.holy_water.events as e
  JOIN holy-water-test-421313.holy_water.orders as o ON e.event_time = o.event_time
),

EventData AS (
  SELECT
    event_time,
    user_id,
    user_params_marketing_id as marketing_id
  FROM `holy-water-test-421313.holy_water.events`
)
SELECT
  i.campaign,
  i.channel,
  i.medium,
  i.ad_content,
  i.ad_group,
  i.total_installs,
  c.total_cost,
  o.category,
  o.order_revenue,
  o.user_id,
FROM CostData AS c
JOIN InstallData AS i ON c.campaign = i.campaign
AND c.channel = i.channel
AND c.medium = i.medium
AND c.ad_content = i.ad_content
AND c.ad_group = i.ad_group
FULL JOIN OrderData AS o ON i.marketing_id = o.marketing_id